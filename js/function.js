$('#carousel1').owlCarousel({    
    margin:0,
	autoplay:true,
	autoplayTimeout:4000,
	smartSpeed:1000,
	dots:true,
    nav:false,
	loop:true,
	responsiveClass:true,
	items:1,
});

$(document).ready(function() {
		$(".flip").click(function() {
			$(".CallusGrid").slideToggle("slow");
		});
	});

/*--- Category Accordian ---*/
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
});


/*--- Accordian ---*/
$(document).ready(function(){
	openCloseControl("accordion1", "handle1", "handleCont1", "panel-title-active", "panel-title");
	openCloseControl("accordion2", "handle2", "handleCont2", "panel-title-active", "panel-title");
	openCloseControl("accordion3", "handle3", "handleCont3", "panel-title-active", "panel-title");
	openCloseControl("accordion4", "handle4", "handleCont4", "panel-title-active", "panel-title");
	openCloseControl("accordion5", "handle5", "handleCont5", "panel-title-active", "panel-title");
	openCloseControl("accordion6", "handle6", "handleCont6", "panel-title-active", "panel-title");
	openCloseControl("accordion7", "handle7", "handleCont7", "panel-title-active", "panel-title");
	openCloseControl("accordion8", "handle8", "handleCont8", "panel-title-active", "panel-title");
	openCloseControl("accordion9", "handle9", "handleCont9", "panel-title-active", "panel-title");

});

function openCloseControl(relParent, relHandle, relContainer, classActive, classNormal)
{
	$("[rel='"+relParent+"']").each(function(){		
	
		var parent = this;
		
		$(parent).find("[rel='"+relHandle+"']").bind("click", function(){			
		
			var container = $(parent).find("[rel='"+relContainer+"']");			
			
			if($(container).css("display") == "none")			{
				$(container).slideDown();
				if(classActive != "")
				{
					$(this).attr("class",classActive);
				}
			}
			else
			{
				$(container).slideUp();
				
				if(classNormal != "")
				{
					$(this).attr("class",classNormal);
				}
			}
		});
	});
}	
	
$(function () {
	$('#datetimepicker1').datetimepicker();
	$('#datetimepicker2').datetimepicker();
	$('#datetimepicker3').datetimepicker();
	$('#datetimepicker4').datetimepicker();
	$('#datetimepicker5').datetimepicker();
	$('#datetimepicker6').datetimepicker();
	$('#datetimepicker7').datetimepicker();
	$('#datetimepicker8').datetimepicker({
		useCurrent: false
	});
	$("#datetimepicker1").on("dp.change", function (e) {
	$('#datetimepicker2').data("DateTimePicker").minDate(e.date);
	});
	$('#PassportExpiry').datetimepicker({
		format: 'DD-MM-YYYY'
	});
	$('#VisaExpiry').datetimepicker({
		format: 'DD-MM-YYYY'
	});
	
});
	
$(document).mouseup(function (e)
{
    var container = $(".CallusGrid");
    if (!container.is(e.target) 
        && container.has(e.target).length === 0)
    {
        $(".CallusGrid").slideUp('slow');
		
    }
});

$(document).on('click','#OpenQuickLink',function(e){
		$("#quickLink").css("display","block");
		$("#quickLink").css("right","0");
		$("#ql-popupDiv1").css("display","block");
		$("#ql-linkDiv1").addClass('active');
		/*$("#ql-popupDiv2").css("display","none");*/
    });	

$(document).on('click','#CloseHelpmeBuy',function(e){
		$("#quickLink").css("display","none");
		$("#quickLink").css("right","-15%");
		$("#ql-popupDiv2").css("display","none");
		$("#ql-popupDiv1").css("display","none");
		$("#ql-linkDiv1").removeClass('active');
		$("#ql-linkDiv2").removeClass('active');
    });	
	
$(document).on('click','#ql-linkDiv2',function(e){
	$("#ql-linkDiv2").addClass('active');
	$("#ql-linkDiv1").removeClass('active');
	$("#ql-popupDiv1").css("display","none");
	$("#ql-popupDiv2").css("display","block");
});
$(document).on('click','#ql-linkDiv1',function(e){
	$("#ql-linkDiv1").addClass('active');
	$("#ql-linkDiv2").removeClass('active');
	$("#ql-popupDiv1").css("display","block");
	$("#ql-popupDiv2").css("display","none");
});


$( "#show_address" ).change(function() {
	// alert( this.value );
	
	$("#address_part_").css("display","none");
	$("#address_part_"+this.value).toggle();
});

$('input[id=lefile]').change(function() {
	$('#photoCover').val($(this).val());
});
$('input[id=lefile2]').change(function() {
	$('#photoCover2').val($(this).val());
});
$('input[id=lefile3]').change(function() {
	$('#photoCover3').val($(this).val());
});

/* Menu quicklink popup open */
$(document).on('click','.shop-helpbuy',function(e){
	$("#quickLink").css("display","block");
	$("#quickLink").css("right","0");
	$("#quickLink, #ql-popupDiv1").addClass('pop-shop');
	//$("#ql-linkDiv1").addClass('pop-shop');
	$("#ql-popupDiv1").css("display","block");
	$("#ql-linkDiv1").addClass('active');
});
$(document).on('click','.shop-quickpayment',function(e){
	$("#quickLink").css("display","block");
	$("#quickLink").css("right","0");
	$("#quickLink, #ql-popupDiv2").addClass('pop-shop');
	//$("#ql-linkDiv2").addClass('pop-shop');
	$("#ql-popupDiv2").css("display","block");
	$("#ql-linkDiv2").addClass('active');
});

$(document).on('click','#search_icon',function(e){
		$("#search_part").css("display","block");
});